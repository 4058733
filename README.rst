ovpn-learnaddress
=================

openvpn learn-address script to manage a hosts-like file

* intended to allow dnsmasq to resolve openvpn clients
* written for openwrt (busybox), but should work most anywhere

How it works?
-------------

Everytime a new user connects to openvpn it will update openvpn hosts in
`/etc/hosts.openvpn-clients`. 

Installation
------------

* Copy this script into `/usb/bin`.
* Set `learn_address` in your openvpn config to `/usr/bin/ovpn-learnaddress`
* Set `script_security` in your openvpn config to `2`
* Include `/etc/hosts.openvpn-clients` in your domain server, 
  like dnsmasq(addn-hosts=/etc/hosts.openvpn-clients)